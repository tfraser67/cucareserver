#-------------------------------------------------
#
# Project created by QtCreator 2012-10-22T19:27:20
#
#-------------------------------------------------

QT       += core gui

TARGET = cuCareServer
TEMPLATE = app


SOURCES += main.cpp\
    socket.cpp \
    ServerSocket.cpp

HEADERS  += server.h \
    socket.h \
    ServerSocket.h

FORMS    += server.ui

